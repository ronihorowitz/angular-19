import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  logout(){
    this.authService.logout().then(value=>{
      this.router.navigate(['/login'])
    })
  }
 

  constructor(private authService:AuthService) { }

  ngOnInit() {
  }

}
