import { Injectable } from '@angular/core';
import { AngularFireAuth} from '@angular/fire/auth';
import { AngularFireDatabase} from '@angular/fire/database';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<firebase.User>;
  
  constructor(private firebaseAuth: AngularFireAuth,
              private db:AngularFireDatabase) {
    this.user = firebaseAuth.authState;
  }

  signup(email: string, password: string) {
    return this.firebaseAuth
      .auth
      .createUserWithEmailAndPassword(email, password)    
  }

  updateProfile(user,name){
    user.updateProfile({displayName:name,photoURL:''})
  }

  login(email: string, password: string){
    return this.firebaseAuth
    .auth
    .signInWithEmailAndPassword(email, password)
  }

  addUser(user,name){
    let uid = user.uid;
    let ref = this.db.database.ref('/');        
    ref.child('user').child(uid).push({name:name}); 
  }

  logout() {   
   return this.firebaseAuth
      .auth
      .signOut()
  } 
}



