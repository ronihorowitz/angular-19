import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email;
  password;
  error = '';

  
  login(){
    this.authService.login(this.email, this.password)
    .then(value=>{
      this.router.navigate(['/']);
    }).catch(err=>{
      console.log(err)
       this.error = err; 
    })
  }

  constructor(public authService: AuthService, public router:Router ) { }

  ngOnInit() {
  }

}
